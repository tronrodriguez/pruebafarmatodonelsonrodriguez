package co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.model;

import java.io.Serializable;

public class Response<T> implements Serializable {
    int code;
    String etag;
    Data<T> data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getEtag() {
        return etag;
    }

    public void setEtag(String etag) {
        this.etag = etag;
    }

    public Data<T> getData() {
        return data;
    }

    public void setData(Data<T> data) {
        this.data = data;
    }
}
