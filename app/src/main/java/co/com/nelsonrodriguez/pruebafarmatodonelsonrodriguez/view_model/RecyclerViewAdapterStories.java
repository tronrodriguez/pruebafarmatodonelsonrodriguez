package co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.view_model;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.R;
import co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.model.Stories;

import static co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.utils.Utils.loguer;

public class RecyclerViewAdapterStories extends RecyclerView.Adapter<RecyclerViewAdapterStories.ViewHolder> {
    private Context ctx;
    private ArrayList<Stories> stories;


    public RecyclerViewAdapterStories(ArrayList<Stories> stories){
        this.stories=stories;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        this.ctx=viewGroup.getContext();
        View view=LayoutInflater.from(ctx).inflate(R.layout.fragment_description_events,viewGroup,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        Stories storie=stories.get(i);
        viewHolder.bindView(storie);

    }

    @Override
    public int getItemCount() {
        return stories.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        View itemView;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.itemView=itemView;
        }
        public void bindView(Stories event){
            loguer(event.toString());
            TextView title=itemView.findViewById(R.id.txtvTitle);
            TextView description=itemView.findViewById(R.id.txtvDescription);
            ImageView thumbnail=itemView.findViewById(R.id.imgvItem);

            title.setText(event.getTitle());
            description.setText(event.getDescription());

            if (event.getThumbnail()!=null){
                String url=event.getThumbnail().getPath()+"/standard_medium."+event.getThumbnail().getExtension();
                Glide.with(ctx).load(url).into(thumbnail);
            }

        }
    }

    private void updateList(Stories event){
        stories.add(event);
        notifyItemInserted(getItemCount());
    }
}
