package co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.view;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;


import java.util.ArrayList;

import co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.R;
import co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.model.Creator;
import co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.model.RetrofitClient;
import co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.model.Response;
import co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.view_model.RecyclerViewAdapterCreators;
import retrofit2.Call;
import retrofit2.Callback;

import static co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.utils.Utils.loguer;


public class CreatorsFragment extends Fragment {
    private ProgressBar progress_circular;
    private RecyclerView recyclerView;
    private ArrayList<Creator> creatorsList;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view=inflater.inflate(R.layout.fragment_comic_list,container,false);
        progress_circular=view.findViewById(R.id.progress_circular);
        recyclerView=view.findViewById(R.id.list);
        loadCreators(view);
        return view;

    }

    private void loadCreators(View view) {
        Call call=RetrofitClient.getClient().getCreators();
        call.enqueue(new Callback<Response<Creator>>() {

            @Override
            public void onResponse(Call<Response<Creator>> call, retrofit2.Response<Response<Creator>> response) {
                creatorsList=(ArrayList<Creator>) response.body().getData().getResults();
                loguer("comicList: "+creatorsList.size());
                progress_circular.setVisibility(View.INVISIBLE);
                setupRecyclerView(recyclerView);
            }

            @Override
            public void onFailure(Call<Response<Creator>> call, Throwable t) {
                progress_circular.setVisibility(View.INVISIBLE);
                Toast.makeText(getContext(),"Error",Toast.LENGTH_SHORT).show();
            }
        });



    }

    private void setupRecyclerView(RecyclerView recyclerView) {
        LinearLayoutManager layoutManager=new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        RecyclerViewAdapterCreators adapter=new RecyclerViewAdapterCreators(creatorsList);
        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(),DividerItemDecoration.VERTICAL));
    }

    public static CreatorsFragment newInstance(){
        return new CreatorsFragment();

    }


}
