package co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.view;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.ArrayList;

import co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.R;
import co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.model.Comic;
import co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.model.Response;
import co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.model.RetrofitClient;
import co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.view_model.MyComicRecyclerViewAdapter;
import retrofit2.Call;
import retrofit2.Callback;

import static co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.utils.Utils.loguer;

public class ComicListFragment extends Fragment {
    private ProgressBar progress_circular;
    private RecyclerView recyclerView;
    private ArrayList<Comic> comicList;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view=inflater.inflate(R.layout.fragment_comic_list,container,false);
        progress_circular=view.findViewById(R.id.progress_circular);
        recyclerView=view.findViewById(R.id.list);
        loadComicsList(view);
        return view;

    }

    private void loadComicsList(final View view) {
        //RetrofitClient.getClient().getAllComics().
        Call call=RetrofitClient.getClient().getAllComics();
        call.enqueue(new Callback<Response<Comic>>() {
            @Override
            public void onResponse(Call<Response<Comic>> call, retrofit2.Response<Response<Comic>> response) {
                comicList= (ArrayList<Comic>) response.body().getData().getResults();
                loguer("comicList: "+comicList.size());
                progress_circular.setVisibility(View.INVISIBLE);
                setupRecyclerView(recyclerView);
            }


            @Override
            public void onFailure(Call call, Throwable t) {
                progress_circular.setVisibility(View.INVISIBLE);
                Toast.makeText(getContext(),"Error",Toast.LENGTH_SHORT).show();


            }
        });
    }

    private void setupRecyclerView(RecyclerView recyclerView) {
        LinearLayoutManager layoutManager=new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        MyComicRecyclerViewAdapter adapter=new MyComicRecyclerViewAdapter(comicList);
        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(),DividerItemDecoration.VERTICAL));
    }

    public static ComicListFragment newInstance() {
        return new ComicListFragment();

    }
}
