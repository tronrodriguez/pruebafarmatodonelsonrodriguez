package co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.view_model;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.R;
import co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.model.Events;

public class RecyclerViewAdapterEvents  extends RecyclerView.Adapter<RecyclerViewAdapterEvents.ViewHolder> {
    private Context ctx;
    private ArrayList<Events> events;

    public RecyclerViewAdapterEvents(ArrayList<Events> events){
        this.events=events;
    }


    @NonNull
    @Override
    public RecyclerViewAdapterEvents.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        this.ctx=viewGroup.getContext();
        View view=LayoutInflater.from(ctx).inflate(R.layout.fragment_description_events,viewGroup,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewAdapterEvents.ViewHolder viewHolder, int i) {
        Events event=events.get(i);
        viewHolder.bindView(event);

    }

    @Override
    public int getItemCount() {
        return events.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        View itemView;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.itemView=itemView;
        }
        public void bindView(Events event){
            TextView title=itemView.findViewById(R.id.txtvTitle);
            TextView description=itemView.findViewById(R.id.txtvDescription);
            ImageView thumbnail=itemView.findViewById(R.id.imgvItem);

            title.setText(event.getTitle());
            description.setText(event.getDescription());

            if (event.getThumbnail()!=null){
                String url=event.getThumbnail().getPath()+"/standard_medium."+event.getThumbnail().getExtension();
                Glide.with(ctx).load(url).into(thumbnail);
            }



        }
    }

    private void updateList(Events event){
        events.add(event);
        notifyItemInserted(getItemCount());
    }

}
