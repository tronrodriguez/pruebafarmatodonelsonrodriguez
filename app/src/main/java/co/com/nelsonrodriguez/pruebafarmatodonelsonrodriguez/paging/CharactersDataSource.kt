package co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.paging

import android.arch.paging.PageKeyedDataSource
import co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.model.API
import co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.model.Character
import co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.model.RetrofitClient
import co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.utils.Constants.FIRST_ADJACENT_PAGE
import co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.utils.Constants.INITIAL_PAGE
import co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.utils.Utils.loguer
import io.reactivex.disposables.CompositeDisposable

class CharactersDataSource(
        private val marvelAPI: API,
        private val compositeDisposable: CompositeDisposable
) : PageKeyedDataSource<Int, Character>() {


    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.model.Character>) {
        val numberOfItems = params.requestedLoadSize
        createObservable(INITIAL_PAGE, FIRST_ADJACENT_PAGE, numberOfItems, callback, null)
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.model.Character>) {
        val page = params.key
        val numberOfItems = params.requestedLoadSize
        createObservable(page, page + 1, numberOfItems, null, callback)
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.model.Character>) {
        val page = params.key
        val numberOfItems = params.requestedLoadSize
        createObservable(page, page - 1, numberOfItems, null, callback)
    }

    private fun createObservable(requestedPage: Int,
                                 adjacentPage: Int,
                                 requestedLoadSize: Int,
                                 initialCallback: LoadInitialCallback<Int, co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.model.Character>?,
                                 callback: LoadCallback<Int, co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.model.Character>?) {
        compositeDisposable.add(
                RetrofitClient.getClient().getAllCharacters(requestedPage * requestedLoadSize)
                        .subscribe({ response ->
                            initialCallback?.onResult(response.data.results, null, adjacentPage)
                            callback?.onResult(response.data.results, adjacentPage)
                        },
                                {
                                    loguer("Error loading page: $requestedPage")
                                })
        )
    }


}