package co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.view_model

import android.arch.lifecycle.ViewModel
import android.arch.paging.PagedList
import android.arch.paging.RxPagedListBuilder
import android.nfc.tech.MifareUltralight.PAGE_SIZE
import co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.model.Character
import co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.model.RetrofitClient
import co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.paging.CharactersDataSourceFactory
import co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.utils.Constants.INITIAL_LOAD_SIZE_HINT
import co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.utils.Constants.PREFETCH_DISTANCE
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class CharactersViewModel :  ViewModel() {

    var characterList: Observable<PagedList<Character>>
    private val compositeDisposable = CompositeDisposable()
    private val sourceFactory: CharactersDataSourceFactory


    init {
        sourceFactory = CharactersDataSourceFactory(compositeDisposable,
                RetrofitClient.getClient())

        val config = PagedList.Config.Builder()
                .setPageSize(PAGE_SIZE)
                .setInitialLoadSizeHint(INITIAL_LOAD_SIZE_HINT)
                .setPrefetchDistance(PREFETCH_DISTANCE)
                .setEnablePlaceholders(false)
                .build()

        characterList = RxPagedListBuilder(sourceFactory, config)
                .setFetchScheduler(Schedulers.io())
                .buildObservable()
                .cache()

    }

    override fun onCleared(){
        super.onCleared()
        compositeDisposable.clear()
    }
}