package co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.view_model;

import android.view.View;

public interface RecyclerViewItemClickListener {
    public void onRecyclerViewClick(View view, int position);
}
