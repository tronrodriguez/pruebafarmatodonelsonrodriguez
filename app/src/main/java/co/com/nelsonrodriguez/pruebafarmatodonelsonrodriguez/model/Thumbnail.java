package co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.model;

import java.io.Serializable;

public class Thumbnail implements Serializable {
    String path;
    String extension;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getUrl(){
        return path+"/standard_medium."+extension;
    }
}
