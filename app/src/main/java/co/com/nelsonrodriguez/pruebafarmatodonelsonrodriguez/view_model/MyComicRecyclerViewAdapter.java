package co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.view_model;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.R;
import co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.model.Comic;

public class MyComicRecyclerViewAdapter extends RecyclerView.Adapter<MyComicRecyclerViewAdapter.ViewHolder> {
    private ArrayList<Comic> comics;
    private Context ctx;

    public MyComicRecyclerViewAdapter(ArrayList<Comic> comics){
        this.comics=comics;
    }

    @NonNull
    @Override
    public MyComicRecyclerViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        this.ctx=viewGroup.getContext();
        View view=LayoutInflater.from(ctx).inflate(R.layout.fragment_comic,viewGroup,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyComicRecyclerViewAdapter.ViewHolder viewHolder, int i) {

        Comic comic=comics.get(i);
        viewHolder.bindView(comic);

    }

    @Override
    public int getItemCount() {
        return comics.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        View itemView;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.itemView=itemView;
        }
        public void bindView(Comic comic){
            TextView title=itemView.findViewById(R.id.comic_title_text_view);
            ImageView thumbnail=itemView.findViewById(R.id.comic_thumbnail_image);
            title.setText(comic.getTitle());
            if (comic.getThumbnail()!=null){
                String url=comic.getThumbnail().getPath()+"/standard_medium."+comic.getThumbnail().getExtension();
                Glide.with(ctx).load(url).into(thumbnail);
            }

        }
    }

    private void updateList(Comic comic){
        comics.add(comic);
        notifyItemInserted(getItemCount());
    }
}
