package co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.view;

import android.arch.lifecycle.ViewModelProviders;
import android.arch.paging.PagedList;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.R;
import co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.model.Character;
import co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.view_model.CharactersAdapter;
import co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.view_model.CharactersViewModel;
import co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.view_model.RecyclerViewItemClickListener;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

import static co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.utils.Utils.loguer;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ListFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ListFragment extends Fragment implements RecyclerViewItemClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private CharactersAdapter adapter;

    private RecyclerView recyclerCharacters;

    private RecyclerView recyclerView;

    private CharactersViewModel viewModel;

    private Disposable disposable;
    private Parcelable recyclerState;

    public ListFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ListFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ListFragment newInstance(String param1, String param2) {
        ListFragment fragment = new ListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        adapter=new CharactersAdapter(this);
        viewModel=ViewModelProviders.of(this).get(CharactersViewModel.class);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_list, container, false);
        recyclerCharacters=view.findViewById(R.id.recyclerCharacters);
        setupRecyclerView(recyclerCharacters);
        return view;
    }

    private void setupRecyclerView(RecyclerView recycler) {
        LinearLayoutManager layoutManager= new LinearLayoutManager(this.getContext());
        recyclerView = recycler;
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        subscribeToList();
    }

    private void subscribeToList() {
        viewModel.getCharacterList()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<PagedList<Character>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(PagedList<Character> characters) {
                        adapter.submitList(characters);
                        if (recyclerState!=null){

                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        loguer(e.getLocalizedMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });


    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onRecyclerViewClick(View view, int position) {
        loguer("onRecyclerViewClick: "+position);
        Character clickedCharacter=adapter.getCurrentList().get(position);
        Bundle bundle=createBundle(clickedCharacter);
        DescriptionFragment descriptionFragment=DescriptionFragment.newInstance("a","b");
        getFragmentManager().beginTransaction().add(R.id.fragment_container,descriptionFragment).addToBackStack(null).commit();
        descriptionFragment.setArguments(bundle);
    }

    private Bundle createBundle(Character character){
        Bundle bundle=new Bundle();
        bundle.putSerializable("character",character);
        return bundle;

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
