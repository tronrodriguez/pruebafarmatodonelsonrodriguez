package co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.utils;

public class Constants {
    public static final String TAG = "farmatodo";
    public static final String TS = "3";
    public static final String MARVEL_BASE_URL = "http://gateway.marvel.com/v1/public/";
    public static final String MARVEL_PUBLIC_API_KEY = "fdba503c7be34befca8d6bd1da01f5db";
    public static final String MARVEL_PRIVATE_API_KEY = "48a7101c7d86b32b4b712e379b6c039766ccdd3c";
    public static final int  INITIAL_PAGE = 0;
    public static final  int FIRST_ADJACENT_PAGE = 1;

    public static final  int PAGE_SIZE = 20;
    public static final  int INITIAL_CHARGE_MULTI = 2;
    public static final  int INITIAL_LOAD_SIZE_HINT = PAGE_SIZE * INITIAL_CHARGE_MULTI;
    public static final  int PREFETCH_DISTANCE = 10;
}
