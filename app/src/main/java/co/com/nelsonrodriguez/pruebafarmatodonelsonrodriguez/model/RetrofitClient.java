package co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.utils.Constants.MARVEL_BASE_URL;
import static co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.utils.Constants.MARVEL_PRIVATE_API_KEY;
import static co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.utils.Constants.MARVEL_PUBLIC_API_KEY;
import static co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.utils.Constants.TS;
import static co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.utils.Utils.getTimestamp;
import static co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.utils.Utils.loguer;
import static co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.utils.Utils.md5;

public class RetrofitClient {
    private static Retrofit retrofit = null;

    public static API getClient(){
        final String ts=getTimestamp();
         String str=ts+MARVEL_PRIVATE_API_KEY+MARVEL_PUBLIC_API_KEY;
        loguer("str: "+str);
        HttpLoggingInterceptor logging=new HttpLoggingInterceptor();

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);
        str=TS+MARVEL_PRIVATE_API_KEY+MARVEL_PUBLIC_API_KEY;
        final String finalStr = str;
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original=chain.request();
                HttpUrl originalHttpUrl=original.url();
                HttpUrl url=originalHttpUrl.newBuilder().
                        addQueryParameter("apikey",MARVEL_PUBLIC_API_KEY)
                        .addQueryParameter("ts",TS)
                        .addQueryParameter("hash",md5(finalStr))
                        .build();
                return chain.proceed(original.newBuilder().url(url).build());
            }
        });

        Gson gson=new GsonBuilder().setLenient().create();
        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl(MARVEL_BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(httpClient.build())
                .build();

        return retrofit.create(API.class);
    }
}
