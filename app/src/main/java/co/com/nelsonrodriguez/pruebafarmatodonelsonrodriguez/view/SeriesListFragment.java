package co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.view;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.ArrayList;

import co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.R;
import co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.model.Response;
import co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.model.RetrofitClient;
import co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.model.Series;
import co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.view_model.RecyclerViewAdapterEvents;
import co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.view_model.RecyclerViewAdapterSeries;
import retrofit2.Call;
import retrofit2.Callback;

public class SeriesListFragment extends Fragment {
    private ProgressBar progress_circular;
    private RecyclerView recyclerView;
    private ArrayList<Series> seriesList;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view=inflater.inflate(R.layout.fragment_comic_list,container,false);
        progress_circular=view.findViewById(R.id.progress_circular);
        recyclerView=view.findViewById(R.id.list);
        loadSeries(view);
        return view;

    }

    private void loadSeries(View view) {
        Call call=RetrofitClient.getClient().getSeries();
        call.enqueue(new Callback<Response<Series>>() {

            @Override
            public void onResponse(Call<Response<Series>> call, retrofit2.Response<Response<Series>> response) {
                seriesList=(ArrayList<Series>)response.body().getData().getResults();
                progress_circular.setVisibility(View.INVISIBLE);
                setupRecyclerView(recyclerView);
            }

            @Override
            public void onFailure(Call<Response<Series>> call, Throwable t) {
                progress_circular.setVisibility(View.INVISIBLE);
                Toast.makeText(getContext(),"Error",Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setupRecyclerView(RecyclerView recyclerView) {
        LinearLayoutManager layoutManager=new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        RecyclerViewAdapterSeries adapter=new RecyclerViewAdapterSeries(seriesList);
        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(),DividerItemDecoration.VERTICAL));
    }

    public static SeriesListFragment newInstance(){
        return new SeriesListFragment();
    }
}
