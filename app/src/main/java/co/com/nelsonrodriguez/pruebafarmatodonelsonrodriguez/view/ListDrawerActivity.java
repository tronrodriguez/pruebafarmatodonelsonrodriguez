package co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.view;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.R;
import co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.model.Series;

import static co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.utils.Utils.loguer;

public class ListDrawerActivity extends AppCompatActivity  implements ListFragment.OnFragmentInteractionListener,DescriptionFragment.OnFragmentInteractionListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loguer("onCreate ListDrawerActivity");
        setContentView(R.layout.activity_list_drawer);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);



        Intent intent = getIntent();
        String type="";
        if (intent.hasExtra("type")){
            type=intent.getStringExtra("type");
        }
        attachFragment(savedInstanceState,type);
    }

    private void attachFragment(Bundle savedInstanceState, String type) {
        if (savedInstanceState == null) {

            switch (type){
                case "characters":
                    this.setTitle("Characters");
                    getSupportFragmentManager().beginTransaction()
                            .add(R.id.fragment_container, ListFragment.newInstance("P1","p2"))
                            .commit();
                    break;
                case "comics":
                    this.setTitle("Comics");
                    getSupportFragmentManager().beginTransaction()
                            .add(R.id.fragment_container, ComicListFragment.newInstance())
                            .commit();
                    break;
                case "creators":
                    this.setTitle("Creators");
                    getSupportFragmentManager().beginTransaction()
                            .add(R.id.fragment_container, CreatorsFragment.newInstance())
                            .commit();
                    break;
                case "events":
                    this.setTitle("Events");
                    getSupportFragmentManager().beginTransaction()
                            .add(R.id.fragment_container, EventListFragment.newInstance())
                            .commit();
                    break;
                case "series":
                    this.setTitle("Series");
                    getSupportFragmentManager().beginTransaction()
                            .add(R.id.fragment_container, SeriesListFragment.newInstance())
                            .commit();
                    break;
                    default:
                        this.setTitle("Stories");
                        getSupportFragmentManager().beginTransaction()
                                .add(R.id.fragment_container, StoriesFragment.newInstance())
                                .commit();
                        break;
            }


        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    protected void onResume() {
        super.onResume();

    }
}
