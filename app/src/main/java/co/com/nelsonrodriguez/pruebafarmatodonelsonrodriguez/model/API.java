package co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.model;

import io.reactivex.Observable;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;


public interface API {
    @GET("characters")
    Observable<Response<Character>> getAllCharacters(@Query("offset") int offset);

    @GET("comics")
    Call<Response<Comic>> getAllComics();

    @GET("creators")
    Call<Response<Creator>> getCreators();

    @GET("events")
    Call<Response<Events>> getEvents();

    @GET("series")
    Call<Response<Series>> getSeries();

    @GET("stories")
    Call<Response<Stories>> getStories();
}
