package co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.paging;

import co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.model.API;
import co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.model.Character;
import io.reactivex.disposables.CompositeDisposable;
import android.arch.paging.DataSource;

public class CharactersDataSourceFactory extends DataSource.Factory<Integer, Character> {
    private CompositeDisposable compositeDisposable;
    private API marvelAPI;


    public CharactersDataSourceFactory(CompositeDisposable compositeDisposable,API marvelAPI){
        this.compositeDisposable=compositeDisposable;
        this.marvelAPI=marvelAPI;
    }


    @Override
    public DataSource<Integer, Character> create() {
        return new CharactersDataSource(marvelAPI,compositeDisposable);
    }
}
