package co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.view;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.R;
import co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.model.Character;
import co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.model.RetrofitClient;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

import static co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.utils.Constants.MARVEL_PRIVATE_API_KEY;
import static co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.utils.Constants.MARVEL_PUBLIC_API_KEY;
import static co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.utils.Constants.TS;
import static co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.utils.Utils.eval;
import static co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.utils.Utils.getTimestamp;
import static co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.utils.Utils.loguer;
import static co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.utils.Utils.md5;


public class MainActivity extends AppCompatActivity implements ListFragment.OnFragmentInteractionListener {
    private String ts="1";
    private String s1="1234";
    private String s2="abcd";
    private Button calcular;
    private EditText edtxtInput;
    private TextView txtvResultado;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        calcular=findViewById(R.id.btnCalcular);
        edtxtInput=findViewById(R.id.edtxtInput);
        txtvResultado=findViewById(R.id.txtvResultado);

        this.setTitle("Prueba Nelson Rodríguez");

        loguer("onCreate MainActivity");
        loguer(getTimestamp());
        String str=TS+MARVEL_PRIVATE_API_KEY+MARVEL_PUBLIC_API_KEY;
        loguer(str);
        loguer(md5(str));

        if( Build.VERSION.SDK_INT >= 9){
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        calcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loguer("calculando...");
                String input=edtxtInput.getText().toString();
                loguer(input);
                if (input.isEmpty()){
                    txtvResultado.setText("Error: Ingresa una operación valida!");
                }else {
                    if (input.contains("+") || input.contains("-") || input.contains("*") || input.contains("/")){
                        if (!input.matches(".*[a-zA-Z]+.*")){
                            boolean whiteSpaceError=false;
                            for (int i=0;i<input.length();i++){

                                if (java.lang.Character.isDigit(input.charAt(i))){
                                    if (i>0){
                                        if (input.charAt(i-1)==' '){
                                            whiteSpaceError=true;
                                        }
                                    }
                                }
                            }
                            if (!whiteSpaceError){
                                int result=(int) eval(input);
                                String res=String.valueOf(result);
                                loguer(res);
                                txtvResultado.setText("Resultado: "+res);
                                showMarvelAPI(result);
                            }else {
                                txtvResultado.setText("Error: no debe haber espacios entre el número y su signo!");
                            }
                        }else {
                            txtvResultado.setText("Error: la operación no puede tener caracteres no númericos!");
                        }
                    }else {
                        txtvResultado.setText("Error: Ingresa una operación valida!");
                    }
                }
            }
        });

        RetrofitClient.getClient().getAllCharacters(0).
                observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.model.Response<Character>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.model.Response<Character> value) {

                        loguer("onNext: "+value.getData().getTotal());
                    }

                    @Override
                    public void onError(Throwable e) {
                        loguer(e.toString());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    private void showMarvelAPI(int result) {
        loguer("Showing result "+result);
        Intent intent=new Intent(this,ListDrawerActivity.class);
        if (result==0){
            loguer("Case 0");
            intent.putExtra("type","characters");

        }else if ((result%3)==0){
            loguer("3 multiples");
            intent.putExtra("type","comics");

        }else if ((result%5)==0){
            loguer("5 multiples");
            intent.putExtra("type","comics");

        }else if ((result%7)==0){
            loguer("7 multiples");
            intent.putExtra("type","creators");

        }else if ((result%11)==0){
            loguer("11 multiples");
            intent.putExtra("type","events");

        }else if ((result%13)==0){
            loguer("13 multiples");
            intent.putExtra("type","series");

        }else {
            loguer("Default case");
            intent.putExtra("type","stories");
        }
        startActivity(intent);

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
