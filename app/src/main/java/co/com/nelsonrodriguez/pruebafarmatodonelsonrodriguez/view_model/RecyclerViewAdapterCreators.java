package co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.view_model;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.R;
import co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.model.Creator;

public class RecyclerViewAdapterCreators extends RecyclerView.Adapter<RecyclerViewAdapterCreators.ViewHolder> {
    private Context ctx;
    private ArrayList<Creator> creators;

    public RecyclerViewAdapterCreators(ArrayList<Creator> creators){
        this.creators=creators;

    }


    @NonNull
    @Override
    public RecyclerViewAdapterCreators.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        this.ctx=viewGroup.getContext();
        View view=LayoutInflater.from(ctx).inflate(R.layout.fragment_comic_description_creators,viewGroup,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewAdapterCreators.ViewHolder viewHolder, int i) {
        Creator creator=creators.get(i);
        viewHolder.bindView(creator);

    }

    @Override
    public int getItemCount() {
        return creators.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        View itemView;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.itemView=itemView;
        }
        public void bindView(Creator creator){
            TextView title=itemView.findViewById(R.id.txtvTitle);
            //TextView description=itemView.findViewById(R.id.txtvDescription);
            ImageView thumbnail=itemView.findViewById(R.id.imgvItem);
            title.setText(creator.getFirstName()+" "+creator.getLastName());
            //description.setText("");
            if (creator.getThumbnail()!=null){
                String url=creator.getThumbnail().getPath()+"."+creator.getThumbnail().getExtension();
                Glide.with(ctx).load(url).into(thumbnail);
            }

        }
    }

    private void updateList(Creator creator){
        creators.add(creator);
        notifyItemInserted(getItemCount());
    }
}
