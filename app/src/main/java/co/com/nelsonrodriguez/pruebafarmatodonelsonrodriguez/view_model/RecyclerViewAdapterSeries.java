package co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.view_model;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.R;
import co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.model.Series;

public class RecyclerViewAdapterSeries extends RecyclerView.Adapter<RecyclerViewAdapterSeries.ViewHolder> {
    private Context ctx;
    private ArrayList<Series> series;

    public RecyclerViewAdapterSeries(ArrayList<Series> series){
        this.series=series;
    }


    @NonNull
    @Override
    public RecyclerViewAdapterSeries.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        this.ctx=viewGroup.getContext();
        View view=LayoutInflater.from(ctx).inflate(R.layout.fragment_description_events,viewGroup,false);
        return new RecyclerViewAdapterSeries.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewAdapterSeries.ViewHolder viewHolder, int i) {
        Series serie=series.get(i);
        viewHolder.bindView(serie);
    }

    @Override
    public int getItemCount() {
        return series.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        View itemView;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.itemView=itemView;
        }
        public void bindView(Series series){
            TextView title=itemView.findViewById(R.id.txtvTitle);
            TextView description=itemView.findViewById(R.id.txtvDescription);
            ImageView thumbnail=itemView.findViewById(R.id.imgvItem);

            title.setText(series.getTitle());
            description.setText(series.getDescription());

            if (series.getThumbnail()!=null){
                String url=series.getThumbnail().getPath()+"/standard_medium."+series.getThumbnail().getExtension();
                Glide.with(ctx).load(url).into(thumbnail);
            }

        }
    }
    private void updateList(Series serie){
        series.add(serie);
        notifyItemInserted(getItemCount());
    }

}
