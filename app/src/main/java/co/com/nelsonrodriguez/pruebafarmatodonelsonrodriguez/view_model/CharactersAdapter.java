package co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.view_model;

import android.arch.paging.PagedListAdapter;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.R;
import co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.model.Character;

import static co.com.nelsonrodriguez.pruebafarmatodonelsonrodriguez.utils.Utils.loguer;

public class CharactersAdapter extends PagedListAdapter<Character,CharactersAdapter.ViewHolder> {
    private Context ctx;
    private RecyclerViewItemClickListener itemClickListener;

    public CharactersAdapter(RecyclerViewItemClickListener itemClickListener){
        super(DIFF_CALLBACK);
        this.itemClickListener=itemClickListener;
    }


    @NonNull
    @Override
    public CharactersAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view=LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_character,viewGroup,false);
        this.ctx=viewGroup.getContext();
        return new CharactersAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CharactersAdapter.ViewHolder viewHolder, final int i) {


        Character character=getItem(i);
        if (character!=null){
            viewHolder.nameEditText.setText(character.getName());
            String url=character.getThumbnail().getPath()+"/standard_medium."+character.getThumbnail().getExtension();
            loguer("image url: "+url);
            Glide.with(ctx).load(url).into(viewHolder.thumbnailImage);
            viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    loguer("on click: "+i);
                    itemClickListener.onRecyclerViewClick(v,i);
                }
            });
        }

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView thumbnailImage;
        private TextView nameEditText;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            thumbnailImage=itemView.findViewById(R.id.thumbnail_image);
            nameEditText=itemView.findViewById(R.id.hero_name_text_view);
        }
    }

    private static DiffUtil.ItemCallback<Character> DIFF_CALLBACK=new DiffUtil.ItemCallback<Character>(){

        @Override
        public boolean areItemsTheSame(@NonNull Character old, @NonNull Character nuevo) {
            //return false;
            return  old.getId()==nuevo.getId();
        }

        @Override
        public boolean areContentsTheSame(@NonNull Character old, @NonNull Character nuevo) {
            //return false;
            return old==nuevo;
        }
    };
}
